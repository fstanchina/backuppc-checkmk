#!/usr/bin/perl
# BackupPC Monitor
# Elly 2014-01-23

use 5.010;
use strict;
use warnings;
use Time::Piece;

use lib "/usr/share/backuppc/lib";
use BackupPC::Lib;
use Data::Dumper;
no utf8;

die("BackupPC::Lib->new failed\n") if ( !(my $bpc = BackupPC::Lib->new) );
#my $TopDir = $bpc->TopDir();
#my $BinDir = $bpc->BinDir();
my %Conf = $bpc->Conf();

$bpc->ChildInit();

my $err = $bpc->ServerConnect($Conf{ServerHost}, $Conf{ServerPort});
if ( $err ) {
        print $err;
        exit(1);
}

my $reply = $bpc->ServerMesg('status hosts');

my %Status = ("dummy" => {"data1" => "data2"});

#say scalar(%Status);
eval $reply;
#say scalar(%Status);

# From backuppc, we only care about last successful backup.

# Set up current date & time
my $now = localtime;

# Flag as Warn/Crit after minimum backup period + this many days
my $warn = 2;
my $crit = 4;

# Loop through the servers
foreach my $k1 (keys %Status){
        my %host_conf = %{$bpc->ConfigDataRead($k1)};

        # Skip fake hosts (admin, trashClean)
        next if not (%host_conf);

        # Skip admin hosts with no XferMethod (BackupPC 4.x)
        next if not exists($host_conf{XferMethod});

        # Skip archive hosts
        next if $host_conf{XferMethod} eq 'archive';

        # Determine minimum backup period
        my $fullPeriod = ($host_conf{FullPeriod}) ? $host_conf{FullPeriod} : $Conf{FullPeriod};
        my $incrPeriod = ($host_conf{IncrPeriod}) ? $host_conf{IncrPeriod} : $Conf{IncrPeriod};
        my $period = ($incrPeriod < $fullPeriod) ? $incrPeriod : $fullPeriod;

        # Only look for last good backup time
        if ($Status{$k1}{"lastGoodBackupTime"}) {
                # Format last good backup time
                my $lgb =  localtime($Status{$k1}{"lastGoodBackupTime"});
                # Calculate days between now and then
                my $diff = ($now - $lgb)->days;
                my $nag = 3;
                my $nice = "";

                # Determine Warn/Crit level; disabled hosts are always OK
                if (($diff < $warn + $period) || $host_conf{BackupsDisable}) {
                        $nag = 0;
                        $nice = "OK";
                } elsif ($diff < $crit + $period) {
                        $nag = 1;
                        $nice = "WARN";
                } else {
                        $nag = 2;
                        $nice = "CRIT";
                }

                # Print Warn/Crit level and server name
                print "$nag backup-$k1 ";

                # Print full backup size
                my @Backups = $bpc->BackupInfoRead($k1);
                my $fullSize = 0;

                for (my $i = 0; $i < @Backups; $i++) {
                  if ($Backups[$i]{type} eq "full") {
                    $fullSize = $Backups[$i]{size};
                  }
                }

                printf('fullsize=%d ', $fullSize);

                # Print nice output
                print $nice;
                if ($diff >= 1) {
                  printf(' - Last backup was %d days and %d hours ago', int $diff, ($diff - int $diff) * 24);
                } else {
                  printf(' - Last backup was %d hours ago', $diff * 24);
                }
        } else {
                print "3 backup-$k1 - This PC has never been backed up";
        }

        if ($host_conf{BackupsDisable}) {
                print " - Automatic backups" if ($host_conf{BackupsDisable} == 1);
                print " - Backups" if ($host_conf{BackupsDisable} == 2);
                print " disabled";
        }

        print "\n";
}
